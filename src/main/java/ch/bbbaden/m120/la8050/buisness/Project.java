package ch.bbbaden.m120.la8050.buisness;

import ch.bbbaden.m120.la8050.model.IModel;

import java.util.ArrayList;
import java.util.List;

public class Project implements IModel {

    private String title;
    private List<Task> tasks = new ArrayList<>();

    public Project(String title){
        this.title = title;
    }

    public int getNumberOfTasks(){
        return (int)tasks.stream().count();
    }
    public void addTask(Task task){
        tasks.add(task);
    }

    public boolean removeTask(Task task){
        return tasks.remove(task);
    }

    public List<Task> getAllTasks(){
        return tasks;
    }

    public void removeAllTask(){
        tasks.clear();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Task> getTasks() {
        return tasks;
    }
}
