package ch.bbbaden.m120.la8050.buisness;

public enum State {
    OPEN,
    IN_PROGRESS,
    DONE,
    CLOSED
}
