package ch.bbbaden.m120.la8050.buisness;

import ch.bbbaden.m120.la8050.model.IModel;

import java.time.LocalDate;

public class Task implements IModel {

    private String title;
    private String description;
    private LocalDate dueDate;
    private State state = State.OPEN;

    public Task(String title, String description, LocalDate dueDate, State state) {
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
        this.state = state;
    }

    public Task(String title, String description){
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public State getState() {
        return state;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public void setState(State state) {
        this.state = state;
    }
}
