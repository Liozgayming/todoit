package ch.bbbaden.m120.la8050.buisness;

public class ProjectTask {
    private final Task task;
    private final Project project;

    public ProjectTask(Task task, Project project) {
        this.task = task;
        this.project = project;
    }


    public Project getProject() {
        return project;
    }

    public Task getTask() {
        return task;
    }
    public String getTaskTitle(){
        return task.getTitle();
    }
}
