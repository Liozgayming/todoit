package ch.bbbaden.m120.la8050.model;

import ch.bbbaden.m120.la8050.buisness.Project;
import ch.bbbaden.m120.la8050.buisness.State;
import ch.bbbaden.m120.la8050.buisness.Task;
import ch.bbbaden.m120.la8050.db.Db;
import ch.bbbaden.m120.la8050.db.IDbListener;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Model implements ITaskModel, IProjectModel, IDbListener {

    private final List<Project> projects;
    private int selectedProjectIndex;
    private int selectedTaskIndex;
    private final PropertyChangeSupport changes = new PropertyChangeSupport(this);
    private final Db db;

    public Model() {
        projects = new ArrayList<>();
        db = new Db(this);
        Thread t = new Thread(db);
        t.start();
    }

    @Override
    public List<Project> getProjects() {
        return projects;
    }

    @Override
    public void addProject(Project p) {
        projects.add(p);
        changes.firePropertyChange("projects", null, projects);
        store();
    }

    public void editProject(Project project) {
        selectedProjectIndex = projects.indexOf(project);
    }

    @Override
    public void saveProject(Project p) {
        projects.set(selectedProjectIndex, p);
        selectedProjectIndex = -1;
        changes.firePropertyChange("projects", null, projects);
        changes.firePropertyChange("saveProject", null, p);
        store();
    }

    @Override
    public void removeProject(Project p) {
        projects.remove(p);
        changes.firePropertyChange("projects", null, projects);
        store();
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        changes.addPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        changes.removePropertyChangeListener(l);
    }

    @Override
    public void addTask(Project p, Task newTask) {
        p.addTask(newTask);
        changes.firePropertyChange("tasks", null, newTask);
        store();
    }


    @Override
    public void editTask(Project p, Task task) {
        selectedTaskIndex = p.getAllTasks().indexOf(task);
    }

    public void saveTask(Project p, Task task) {
        p.getAllTasks().set(selectedTaskIndex, task);
        selectedTaskIndex = -1;
        changes.firePropertyChange("tasks", null, p.getAllTasks());
        changes.firePropertyChange("saveTask", null, p);
        store();
    }

    @Override
    public void removeTask(Project p, Task task) {
        p.removeTask(task);
        changes.firePropertyChange("tasks", null, task);
        store();
    }

    @Override
    public void deleteAllTasks(Project p) {
        p.removeAllTask();
        store();
    }

    @Override
    public List<Task> getAllTasks(Project p) {
        if (p != null){
            return p.getAllTasks();
        }
        return new ArrayList<>();
    }

    protected void store() {
        db.store(export());
    }

    protected Object export() {
        HashMap<String, ArrayList<?>> result = new HashMap<>();

        ArrayList<HashMap<String, String>> projects = new ArrayList<>();
        result.put("projects", projects);
        ArrayList<HashMap<String, Object>> tasks = new ArrayList<HashMap<String, Object>>();
        result.put("tasks", tasks);
        for (int i = 0; i < this.projects.size(); i++) {
            HashMap<String, String> hashProject = new HashMap<>();
            hashProject.put("title", this.projects.get(i).getTitle());
            projects.add(hashProject);
            for (Task task : this.projects.get(i).getAllTasks()) {
                HashMap<String, Object> hashTask = new HashMap<>();
                hashTask.put("title", task.getTitle());
                hashTask.put("description", task.getDescription());
                if (task.getDueDate() != null){
                    hashTask.put("dueDate", task.getDueDate().toString());
                } else {
                    hashTask.put("dueDate", null);
                }
                hashTask.put("state", task.getState().name());
                hashTask.put("projectIndex", i);
                tasks.add(hashTask);
            }
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onDbChanged(HashMap<String, ?> map) {
        projects.clear();
        ArrayList<HashMap<String, String>> projects = (ArrayList<HashMap<String, String>>) map.get("projects");
        for (HashMap<String, String> project : projects) {
            this.projects.add(new Project(project.get("title")));
        }
        ArrayList<HashMap<String, Object>> tasks = (ArrayList<HashMap<String, Object>>) map.get("tasks");
        for (HashMap<String, Object> task : tasks) {
            LocalDate date = null;
            if (task.containsKey("dueDate")){
               date = LocalDate.parse((CharSequence) task.get("dueDate"));
            }
            Task newTask = new Task((String) task.get("title"), (String) task.get("description"),date, State.valueOf((String) task.get("state")));
            this.projects.get(Math.toIntExact((Long) task.get("projectIndex"))).addTask(newTask);
        }
        changes.firePropertyChange("projects", null, projects);
    }
}
