/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m120.la8050.model;

import ch.bbbaden.m120.la8050.buisness.Project;

import java.beans.PropertyChangeListener;
import java.util.List;

/**
 *
 * @author Manuel Bachofner
 */
public interface IProjectModel extends IModel {

    /**
     *
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l);

    /**
     *
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l);

    /**
     *
     * @return
     */
    public abstract List<Project> getProjects();

    /**
     *
     * @param p
     */
    public abstract void addProject(Project p);

    /**
     *
     * @param p
     */
    public abstract void saveProject(Project p);

    /**
     *
     * @param p
     */
    public abstract void removeProject(Project p);

}
