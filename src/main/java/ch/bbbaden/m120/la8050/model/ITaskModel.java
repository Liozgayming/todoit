/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m120.la8050.model;

import ch.bbbaden.m120.la8050.buisness.Project;
import ch.bbbaden.m120.la8050.buisness.Task;

import java.beans.PropertyChangeListener;
import java.util.List;

/**
 *
 * @author Manuel Bachofner
 */
public interface ITaskModel extends IModel {

    /**
     *
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l);

    /**
     *
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l);

    /**
     *
     * @param p
     * @param newTask
     */
    public abstract void addTask(Project p, Task newTask);

    /**
     *
     * @param p
     * @param newTask
     */
    public abstract void editTask(Project p, Task newTask);

    /**
     *
     * @param p
     * @param task
     */
    public abstract void removeTask(Project p, Task task);

    /**
     *
     * @param p
     */
    public abstract void deleteAllTasks(Project p);

    /**
     *
     * @param p
     * @return
     */
    public abstract List<Task> getAllTasks(Project p);

}
