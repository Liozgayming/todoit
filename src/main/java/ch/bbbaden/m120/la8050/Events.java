package ch.bbbaden.m120.la8050;

public enum Events {
    tesz("gest");

    private final String name;

    Events(String name) {
        this.name = name;
    }

    public String getNAME() {
        return name;
    }
}
