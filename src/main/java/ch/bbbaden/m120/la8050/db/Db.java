package ch.bbbaden.m120.la8050.db;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.Executor;

public class Db implements Runnable {

    private final IDbListener listener;
    private DatabaseReference ref;

    public Db(IDbListener listener) {
        this.listener = listener;
    }

    public void run() {
        FireBaseService fbs = null;
        try {
            fbs = new FireBaseService();
        } catch (IOException e) {
            e.printStackTrace();
        }


        ref = fbs.getDb()
                .getReference("/");
        ref.addValueEventListener(new ValueEventListener() {

            @SuppressWarnings("unchecked")
            public void onDataChange(DataSnapshot dataSnapshot) {
                Object document = dataSnapshot.getValue();
                listener.onDbChanged((HashMap<String, ?>) document);
            }


            public void onCancelled(DatabaseError error) {
                System.out.print("Error: " + error.getMessage());
            }
        });
    }

    public void store(Object val) {
        ref.setValueAsync(val).addListener(() -> {
            System.out.println("a");
        }, command -> {
            System.out.println("b");
        });
    }
}
