package ch.bbbaden.m120.la8050.db;

import java.util.HashMap;

public interface IDbListener {
    void onDbChanged(HashMap<String, ?> map);
}
