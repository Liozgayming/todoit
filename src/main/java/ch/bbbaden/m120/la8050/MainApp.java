package ch.bbbaden.m120.la8050;

import ch.bbbaden.m120.la8050.model.Model;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainApp extends Application {

    @Override
        public void start(Stage stage){
            ViewController.getInstance().setModel(new Model());
            ViewController.getInstance().setStage(stage);
            ViewController.getInstance().showInitialWindowForm();
        }

        /**
         * @param args the command line arguments
         */
        public static void main(String[] args) {
            launch(args);
        }
}
