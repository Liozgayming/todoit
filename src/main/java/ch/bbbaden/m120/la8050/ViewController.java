package ch.bbbaden.m120.la8050;

import ch.bbbaden.m120.la8050.buisness.Project;
import ch.bbbaden.m120.la8050.buisness.Task;
import ch.bbbaden.m120.la8050.model.Model;
import ch.bbbaden.m120.la8050.view.IController;
import ch.bbbaden.m120.la8050.viewmodel.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.Console;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ViewController {

    private static final ViewController instance= new ViewController();
    private Stage stage;
    private Model model;

    private ViewController(){
    }

    public void showProjectList() {
        loadView("ProjectList.fxml", new ProjectListViewModel(model), this.stage);
    }

    public void showInitialWindowForm(){
        loadView("InitiallWindow.fxml", new InitialWindowViewModel(model),  this.stage);
    }


    public void showProjectForm(Project selectedProject){
        if (selectedProject != null){
            loadView("ProjectForm.fxml", new ProjectFormViewModel(model, selectedProject),  this.stage);
        }
    }

    public void showTask(Project selectedProject){
        Stage stage = new Stage();
        loadView("Task.fxml", new TaskViewModel(selectedProject, model, stage),  stage);
    }

    public void showTask(Task task, Project selectedProject){
        Stage stage = new Stage();
        loadView("Task.fxml", new TaskViewModel(task, selectedProject, model, stage), stage);
    }

    public void showProject(Project project){
        Stage stage = new Stage();
        loadView("Project.fxml", new ProjectViewModel(project, model, stage), stage);
    }

    public void showProject(){
        Stage stage = new Stage();
        loadView("Project.fxml", new ProjectViewModel(model, stage), stage);
    }

    private void loadView(String path, IViewModel viewModel, Stage stage) {

        final FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(path));
        try {
            final Parent root = loader.load();
            final IController controller = loader.getController();
            controller.setViewModel(viewModel);
            model.addPropertyChangeListener(viewModel);

            final Scene scene = new Scene(root);
            scene.getStylesheets().add("styles.css");
            stage.setScene(scene);
            stage.setTitle(controller.getTitle());
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public static ViewController getInstance() {
        return instance;
    }
}
