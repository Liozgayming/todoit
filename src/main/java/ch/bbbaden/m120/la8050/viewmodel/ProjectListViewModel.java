package ch.bbbaden.m120.la8050.viewmodel;

import ch.bbbaden.m120.la8050.ViewController;
import ch.bbbaden.m120.la8050.buisness.Project;
import ch.bbbaden.m120.la8050.buisness.Task;
import ch.bbbaden.m120.la8050.model.IProjectModel;
import ch.bbbaden.m120.la8050.model.Model;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sun.awt.windows.ThemeReader;

import javax.jws.WebParam;
import java.beans.PropertyChangeEvent;
import java.time.LocalDate;
import java.util.Random;

public class ProjectListViewModel extends ViewModel<IProjectModel> {

    private ObservableList<Project> projects;
    private BooleanProperty selected = new SimpleBooleanProperty();
    private StringProperty projectTitle = new SimpleStringProperty();
    private StringProperty projectTaskSize = new SimpleStringProperty();
    private Project selectedProject;

    public ProjectListViewModel(IProjectModel model){
        setModel(model);
    }

    public ObservableList<Project> getProjects() {
        return projects;
    }

    public StringProperty getProjectTaskSize() {
        return projectTaskSize;
    }

    public void setSelectedProject(Project selectedProject) {
        if (selectedProject != null) {
            this.selectedProject = selectedProject;
            projectTitle.setValue(selectedProject.getTitle());
            projectTaskSize.setValue("Aufwand: " + selectedProject.getNumberOfTasks());
        } else{
            this.selectedProject = null;
            projectTitle.setValue("");
            projectTaskSize.setValue("");

        }
        isSelected().set(selectedProject == null);
    }

    public StringProperty getProjectTitle() {
        return projectTitle;
    }

    public void editAction(){
        ViewController.getInstance().showProject(selectedProject);
    }

    public void removeAction(){
        model.removeProject(selectedProject);
    }

    public BooleanProperty getSelectedProperty(){
        return selected;
    }

    public void showTasks(){
        ViewController.getInstance().showProjectForm(selectedProject);
    }


    public void addProject(){
        ViewController.getInstance().showProject();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case "projects":
                this.projects.setAll(model.getProjects());
            default:
        }
    }
    @Override
    void UpdateProperties() {
        this.projects = FXCollections.observableArrayList(model.getProjects());
        selected.setValue(true);
        setSelectedProject(null);
    }

    public BooleanProperty isSelected() {
        return selected;
    }
}
