package ch.bbbaden.m120.la8050.viewmodel;

import ch.bbbaden.m120.la8050.ViewController;
import ch.bbbaden.m120.la8050.buisness.Project;
import ch.bbbaden.m120.la8050.buisness.ProjectTask;
import ch.bbbaden.m120.la8050.buisness.State;
import ch.bbbaden.m120.la8050.buisness.Task;
import ch.bbbaden.m120.la8050.model.Model;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.beans.PropertyChangeEvent;
import java.util.Comparator;
import java.util.stream.Collectors;

public class InitialWindowViewModel extends ViewModel<Model>{

    ObservableList<Project> projects;
    ObservableList<Task> tasks;
    ObservableList<ProjectTask> topTasks;

    Project selectedProject;
    Task selectedTask;

    StringProperty projectTitle = new SimpleStringProperty();
    StringProperty taskCount = new SimpleStringProperty();

    BooleanProperty IsNoProjektSelected = new SimpleBooleanProperty(true);
    BooleanProperty IsNoTaskSelected = new SimpleBooleanProperty(true);


    public InitialWindowViewModel(Model model){
        setModel(model);
    }

    @Override
    void UpdateProperties() {
        this.projects = FXCollections.observableArrayList(model.getProjects());
        this.topTasks = FXCollections.observableArrayList();
        this.tasks = FXCollections.observableArrayList();
        UpdateTopTasks();
        UpdateButtons();
    }

    private void UpdateTopTasks() {
        model.getProjects().forEach(p -> p.getTasks().forEach(task -> this.topTasks.add(new ProjectTask(task, p))));
    }

    private void UpdateButtons(){
        isNoProjektSelected().set(selectedProject == null);
        isNoTaskSelected().set(selectedTask == null);
    }

    public ObservableList<Project> getProjects(){
        return projects;
    }

    public ObservableList<ProjectTask> getTopTasks(){
        return topTasks;
    }

    public ObservableList<Task> getTask(){
        return tasks;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case "projects":
                this.projects.setAll(model.getProjects());
                break;
                case "tasks":
                    UpdateProject(selectedProject);
            default:
        }
        UpdateButtons();
        UpdateTopTasks();
    }

    public BooleanProperty isNoProjektSelected() {
        return IsNoProjektSelected;
    }

    public BooleanProperty isNoTaskSelected() {
        return IsNoTaskSelected;
    }

    public void ProjectNewAction() {
        ViewController.getInstance().showProject();
    }

    public void ProjectDeleteAction() {
        model.removeProject(selectedProject);
    }

    public void TaskNewAction() {
        ViewController.getInstance().showTask(selectedProject);
    }

    public void ProjectEditAction() {
        ViewController.getInstance().showProject(selectedProject);
    }
    public void TaskDeleteAction() {
        model.removeTask(selectedProject, selectedTask);
    }

    public void setSelectedTask(Task selectedTask) {
        this.selectedTask = selectedTask;
        UpdateButtons();
    }


    public void setSelectedProject(Project selectedProject) {
        UpdateProject(selectedProject);
    }

    private void UpdateProject(Project selectedProject) {
        this.selectedProject = selectedProject;
        if (selectedProject != null) {
            projectTitle.setValue(selectedProject.getTitle());
            taskCount.setValue("Tasks: " + selectedProject.getNumberOfTasks());
        }
        UpdateTask();
        UpdateButtons();
    }

    private void UpdateTask() {
        this.tasks.setAll(model.getAllTasks(selectedProject).stream().filter(task -> task.getState() == State.OPEN).sorted(Comparator.comparing(Task::getDueDate)).collect(Collectors.toList()));
    }

    public StringProperty getTaskCount() {
        return taskCount;
    }

    public StringProperty getProjectTitle() {
        return projectTitle;
    }
}
