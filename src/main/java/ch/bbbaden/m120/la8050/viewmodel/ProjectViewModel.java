package ch.bbbaden.m120.la8050.viewmodel;

import ch.bbbaden.m120.la8050.ViewController;
import ch.bbbaden.m120.la8050.buisness.Project;
import ch.bbbaden.m120.la8050.buisness.Task;
import ch.bbbaden.m120.la8050.model.IProjectModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.stage.Stage;

import java.beans.PropertyChangeEvent;

public class ProjectViewModel extends ViewModel<Project>{

    private StringProperty title = new SimpleStringProperty();
    private IProjectModel projectModel;
    private Project startProject;
    private Stage stage;

    public ProjectViewModel(IProjectModel projectModel, Stage stage){
        this.stage = stage;
        this.projectModel = projectModel;
        setModel(new Project(""));
    }

    public ProjectViewModel(Project project, IProjectModel projectModel, Stage stage){
        this.stage = stage;
        this.startProject = project;

        Project projectClone = new Project(project.getTitle());
        for (Task task: project.getTasks()
             ) {
            projectClone.addTask(task);
        }

        setModel(projectClone);
        this.projectModel = projectModel;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
    }

    @Override
    void UpdateProperties() {
        title.setValue(model.getTitle());
        title.addListener((observable, oldValue, newValue) -> {model.setTitle(newValue);});
    }

    public StringProperty getTitle() {
        return title;
    }

    public void CancelAction(){
        stage.close();
    }

    public void SafeAction(){
        if (startProject != null){
            projectModel.removeProject(startProject);
        }
        projectModel.addProject(model);
        stage.close();
    }
}
