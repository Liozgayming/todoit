package ch.bbbaden.m120.la8050.viewmodel;

import ch.bbbaden.m120.la8050.model.IModel;

public abstract class ViewModel<T extends IModel> implements IViewModel{

    protected T model;

    @Override
    public void setModel(IModel model) {
        this.model = (T)model;
        UpdateProperties();
    }

    public IModel getModel(){
        return model;
    }

    abstract void UpdateProperties();
}
