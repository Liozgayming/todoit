package ch.bbbaden.m120.la8050.viewmodel;

import ch.bbbaden.m120.la8050.ViewController;
import ch.bbbaden.m120.la8050.buisness.Project;
import ch.bbbaden.m120.la8050.buisness.State;
import ch.bbbaden.m120.la8050.buisness.Task;
import ch.bbbaden.m120.la8050.model.ITaskModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.stage.Stage;

import javax.lang.model.type.NullType;
import java.beans.PropertyChangeEvent;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

public class TaskViewModel extends ViewModel<Task> {
    private StringProperty title = new SimpleStringProperty();
    private StringProperty description = new SimpleStringProperty();
    private ObjectProperty<LocalDate> dueDate = new SimpleObjectProperty<>();
    private ObjectProperty state = new SimpleObjectProperty();
    private ITaskModel taskModel;
    private Project project;
    private Task startTask;
    private Stage stage;

    public StringProperty getTitle() {
        return title;
    }

    public StringProperty getDescription() {
        return description;
    }

    public ObjectProperty<LocalDate> getDueDate() {
        return dueDate;
    }

    public ObjectProperty getState() {
        return state;
    }

    public TaskViewModel(Task task, Project project, ITaskModel taskModel, Stage stage){
        this.taskModel = taskModel;
        this.stage = stage;
        this.project = project;
        Task taskClone = new Task(task.getTitle(), task.getDescription());
        taskClone.setDueDate(task.getDueDate());
        taskClone.setState(task.getState());
        this.startTask = task;
        setModel(taskClone);
    }

    public TaskViewModel(Project project, ITaskModel taskModel, Stage stage){
        this.taskModel = taskModel;
        this.stage = stage;
        setModel(new Task("",""));
        this.project = project;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case "title":
                this.title.setValue(model.getTitle());
                break;
            case "description":
                this.description.setValue(model.getDescription());
                break;
            case "dueDate":
                this.dueDate.setValue(model.getDueDate());
                break;
            case "state":
                this.state.setValue(model.getState().toString());
                break;
            default:
        }
    }

    @Override
    void UpdateProperties() {
        title.addListener((observable, oldValue, newValue) -> model.setTitle(newValue));
        description.addListener((observable, oldValue, newValue) -> model.setDescription(newValue));
        dueDate.addListener((observable, oldValue, newValue) -> model.setDueDate(newValue));
        state.addListener((observable, oldValue, newValue) -> model.setState((State) newValue));

        this.title.setValue(model.getTitle());
        this.description.setValue(model.getDescription());
        this.dueDate.setValue(model.getDueDate());
        this.state.setValue(model.getState());
    }

    public void CancelAction(){
        stage.close();
    }
    public void SafeAction(){
        if (startTask != null){
            taskModel.removeTask(project, startTask);
        }
        taskModel.addTask(project, model);
        stage.close();
    }
}
