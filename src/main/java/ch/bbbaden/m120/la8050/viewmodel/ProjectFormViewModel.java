package ch.bbbaden.m120.la8050.viewmodel;

import ch.bbbaden.m120.la8050.ViewController;
import ch.bbbaden.m120.la8050.buisness.Project;
import ch.bbbaden.m120.la8050.buisness.Task;
import ch.bbbaden.m120.la8050.model.ITaskModel;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.beans.PropertyChangeEvent;
import java.time.LocalDate;

public class ProjectFormViewModel extends ViewModel<ITaskModel> {
    private ObservableList<Task> tasks = FXCollections.observableArrayList();
    private BooleanProperty selected = new SimpleBooleanProperty(true);
    private Task selectedTask;

    private StringProperty title = new SimpleStringProperty();
    private StringProperty description = new SimpleStringProperty();
    private StringProperty dueDate = new SimpleStringProperty();
    private StringProperty state = new SimpleStringProperty();
    private Project activeProject;

    public ProjectFormViewModel(ITaskModel model, Project activeProject) {
        this.activeProject = activeProject;
        setModel(model);
    }

    public ObservableList<Task> getTasks() {
        return tasks;
    }

    public void removeAction() {
        model.removeTask(activeProject, selectedTask);
    }

    public void back() {
        ViewController.getInstance().showProjectList();
    }

    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case "tasks":
                this.tasks.setAll(model.getAllTasks(activeProject));
            default:
        }
    }

    public void addTask() {

    }

    public void setSelectedTask(Task selectedTask) {
        if (selectedTask != null) {
            this.selectedTask = selectedTask;

            title.setValue(selectedTask.getTitle());
            description.setValue(selectedTask.getDescription());
            if (selectedTask.getDueDate() != null) {
                dueDate.setValue(selectedTask.getDueDate().toString());
            }
            if (selectedTask.getState() != null) {
                state.setValue(selectedTask.getState().toString());
            }
        } else {
            this.selectedTask = null;
            title.setValue("");
            description.setValue("");
            dueDate.setValue(LocalDate.now().toString());
            state.setValue("");
        }
        selected.setValue(selectedTask == null);
    }

    public StringProperty getTitle() {
        return title;
    }

    public StringProperty getDescription() {
        return description;
    }

    public StringProperty getDueDate() {
        return dueDate;
    }

    public StringProperty getState() {
        return state;
    }

    public BooleanProperty isSelected() {
        return selected;
    }

    public void editAction() {
        ViewController.getInstance().showTask(selectedTask, activeProject);
    }

    public void newAction() {
        ViewController.getInstance().showTask(activeProject);
    }

    @Override
    void UpdateProperties() {
        tasks = FXCollections.observableArrayList(model.getAllTasks(activeProject));
    }
}

