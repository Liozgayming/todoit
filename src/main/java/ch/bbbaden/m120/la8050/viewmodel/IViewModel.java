package ch.bbbaden.m120.la8050.viewmodel;

import ch.bbbaden.m120.la8050.model.IModel;

import java.beans.PropertyChangeListener;

public interface IViewModel extends PropertyChangeListener {
    void setModel(IModel model);

}
