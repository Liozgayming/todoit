package ch.bbbaden.m120.la8050.view;

import ch.bbbaden.m120.la8050.buisness.Project;
import ch.bbbaden.m120.la8050.buisness.Task;
import ch.bbbaden.m120.la8050.model.Model;
import ch.bbbaden.m120.la8050.viewmodel.IViewModel;
import ch.bbbaden.m120.la8050.viewmodel.ProjectListViewModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

import java.time.LocalDate;
import java.util.Random;


public class ProjectListController extends Controller<ProjectListViewModel> {

    @FXML
    public Button TasksBtn;
    @FXML
    public Button DeleteBtn;
    @FXML
    public Button EditBtn;
    @FXML
    public Button NewBtn;
    @FXML
    public ListView ProjectsLV;
    @FXML
    public Label ProjectNameLbl;
    @FXML
    public Label TaskCountLbl;

    @FXML
    public void initialize() {
        ProjectsLV.setCellFactory(new Callback<ListView<Project>, ListCell<Project>>() {

            @Override
            public ListCell<Project> call(ListView<Project> param) {
                ListCell<Project> cell = new ListCell<Project>() {
                    @Override
                    protected void updateItem(Project item, boolean bln) {
                        super.updateItem(item, bln);
                        if (item != null) {
                            setText(item.getTitle());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
    }

    public void bind() {
        ProjectNameLbl.textProperty().bind(viewModel.getProjectTitle());
        TaskCountLbl.textProperty().bind(viewModel.getProjectTaskSize());
        ProjectsLV.setItems(viewModel.getProjects());
        ProjectsLV.getSelectionModel().selectedItemProperty().addListener((ChangeListener<Project>) (observable, oldValue, newValue) -> viewModel.setSelectedProject(newValue));
        TasksBtn.disableProperty().bind(viewModel.isSelected());
        DeleteBtn.disableProperty().bind(viewModel.isSelected());
        EditBtn.disableProperty().bind(viewModel.isSelected());
    }

    public void tasksAction(ActionEvent actionEvent) {
        viewModel.showTasks();
    }

    public void deleteAction(ActionEvent actionEvent) {
        viewModel.removeAction();
    }

    public void editAction(ActionEvent actionEvent) {
        viewModel.editAction();
    }

    public void newAction(ActionEvent actionEvent) {
        viewModel.addProject();
    }

    @Override
    public String getTitle() {
        return "Projects";
    }

}
