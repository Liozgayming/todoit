package ch.bbbaden.m120.la8050.view;

import ch.bbbaden.m120.la8050.buisness.State;
import ch.bbbaden.m120.la8050.viewmodel.IViewModel;
import ch.bbbaden.m120.la8050.viewmodel.TaskViewModel;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class TaskController extends Controller<TaskViewModel> {
    @FXML
    public TextField titelLbl;
    @FXML
    public DatePicker date;
    @FXML
    public TextArea describtionTb;
    @FXML
    public ComboBox stateCb;

    public void bind(){
        stateCb.setItems( FXCollections.observableArrayList(State.values()));
        stateCb.valueProperty().bindBidirectional(viewModel.getState());
        this.titelLbl.textProperty().bindBidirectional(viewModel.getTitle());
        this.describtionTb.textProperty().bindBidirectional(viewModel.getDescription());
        date.valueProperty().bindBidirectional(viewModel.getDueDate());
    }

    public void cancelAction(ActionEvent actionEvent) {
        viewModel.CancelAction();
    }

    public void safeAction(ActionEvent actionEvent) {
        viewModel.SafeAction();
    }

    @Override
    public String getTitle() {
        return "Edit Task";
    }
}
