package ch.bbbaden.m120.la8050.view;

import ch.bbbaden.m120.la8050.viewmodel.IViewModel;

public abstract class Controller<T extends IViewModel> implements IController{

    protected T viewModel;

    @Override
    public void setViewModel(IViewModel viewModel) {
        this.viewModel = (T)viewModel;
        this.bind();
    }
}
