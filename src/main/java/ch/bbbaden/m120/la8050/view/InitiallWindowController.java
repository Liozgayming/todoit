package ch.bbbaden.m120.la8050.view;

import ch.bbbaden.m120.la8050.ViewController;
import ch.bbbaden.m120.la8050.buisness.Project;
import ch.bbbaden.m120.la8050.buisness.ProjectTask;
import ch.bbbaden.m120.la8050.buisness.Task;
import ch.bbbaden.m120.la8050.viewmodel.InitialWindowViewModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

public class InitiallWindowController extends Controller<InitialWindowViewModel> {
    @FXML
    private ListView TasksLV;
    @FXML
    private Button taskDeleteBtn;
    @FXML
    private Button projektDeleteBtn;
    @FXML
    private Button taskNewBtn;
    @FXML
    private Button projektEditBtn;
    @FXML
    private Button projektNewBtn;
    @FXML
    private Button taskEditBtn;
    @FXML
    private Label projectTaskCountLbl;
    @FXML
    private Label projectNameLbl;
    @FXML
    private TableView TopTasksTV;
    @FXML
    private TableColumn TopTasksAcceptTC;
    @FXML
    private TableColumn TopTasksTitleTC;
    @FXML
    private ListView projektLV;

    @Override
    public String getTitle() {
        return "ToDoIt";
    }

    @Override
    public void bind() {
        projektDeleteBtn.disableProperty().bind(viewModel.isNoProjektSelected());
        projektEditBtn.disableProperty().bind(viewModel.isNoProjektSelected());

        taskDeleteBtn.disableProperty().bind(viewModel.isNoTaskSelected());
        taskEditBtn.disableProperty().bind(viewModel.isNoTaskSelected());

        projektLV.setItems(viewModel.getProjects());
        projektLV.getSelectionModel().selectedItemProperty().addListener((ChangeListener<Project>) (observable, oldValue, newValue) -> viewModel.setSelectedProject(newValue));

        TasksLV.setItems(viewModel.getTask());
        TasksLV.getSelectionModel().selectedItemProperty().addListener((ChangeListener<Task>) (observable, oldValue, newValue) -> viewModel.setSelectedTask(newValue));

        TopTasksTV.setItems(viewModel.getTopTasks());

        projectNameLbl.textProperty().bind(viewModel.getProjectTitle());
        projectTaskCountLbl.textProperty().bind(viewModel.getTaskCount());
    }

    @Deprecated
    public void initialize() {
        projektLV.setCellFactory(new Callback<ListView<Project>, ListCell<Project>>() {

            @Override
            public ListCell<Project> call(ListView<Project> param) {
                ListCell<Project> cell = new ListCell<Project>() {
                    @Override
                    protected void updateItem(Project item, boolean bln) {
                        super.updateItem(item, bln);
                        if (item != null) {
                            setText(item.getTitle());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
        TasksLV.setCellFactory(new Callback<ListView<Task>, ListCell<Task>>() {

            @Override
            public ListCell<Task> call(ListView<Task> param) {
                ListCell<Task> cell = new ListCell<Task>() {
                    @Override
                    protected void updateItem(Task item, boolean bln) {
                        super.updateItem(item, bln);
                        if (item != null) {
                            setText(item.getTitle());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });

        TopTasksTitleTC.setCellValueFactory(new PropertyValueFactory<>("TaskTitle"));
        addButtonToTable();
    }

    private void addButtonToTable() {
        TableColumn<ProjectTask, Void> colBtn = new TableColumn("Annehmen");

        Callback<TableColumn<ProjectTask, Void>, TableCell<ProjectTask, Void>> cellFactory = new Callback<TableColumn<ProjectTask, Void>, TableCell<ProjectTask, Void>>() {
            @Override
            public TableCell<ProjectTask, Void> call(final TableColumn<ProjectTask, Void> param) {
                final TableCell<ProjectTask, Void> cell = new TableCell<ProjectTask, Void>(){
                    private final Button btn = new Button("Annehmen");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            ViewController.getInstance().showTask(getTableView().getItems().get(getIndex()).getTask(), getTableView().getItems().get(getIndex()).getProject());
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        TopTasksTV.getColumns().add(colBtn);

    }
    @FXML
    public void ProjectNewAction(ActionEvent actionEvent) {
        viewModel.ProjectNewAction();
    }

    @FXML
    public void ProjectDeleteAction(ActionEvent actionEvent) {
        viewModel.ProjectDeleteAction();
    }

    @FXML
    public void TaskNewAction(ActionEvent actionEvent) {
        viewModel.TaskNewAction();
    }

    @FXML
    public void ProjectEditAction(ActionEvent actionEvent) {
        viewModel.ProjectEditAction();
    }

    @FXML
    public void TaskDeleteAction(ActionEvent actionEvent) {
        viewModel.TaskDeleteAction();
    }

    @FXML
    public void TaskEditBtn(ActionEvent actionEvent) {
    }
}
