package ch.bbbaden.m120.la8050.view;

import ch.bbbaden.m120.la8050.viewmodel.IViewModel;

public interface IController {
    void setViewModel(IViewModel viewModel);
    String getTitle();
    void bind();
}
