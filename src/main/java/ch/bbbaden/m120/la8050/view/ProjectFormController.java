package ch.bbbaden.m120.la8050.view;

import ch.bbbaden.m120.la8050.buisness.Project;
import ch.bbbaden.m120.la8050.buisness.State;
import ch.bbbaden.m120.la8050.buisness.Task;
import ch.bbbaden.m120.la8050.viewmodel.IViewModel;
import ch.bbbaden.m120.la8050.viewmodel.ProjectFormViewModel;
import ch.bbbaden.m120.la8050.viewmodel.ProjectListViewModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.util.Date;

public class ProjectFormController extends Controller<ProjectFormViewModel> {

    @FXML
    public Button backBtn;
    @FXML
    public Button deleteBtn;
    @FXML
    public Button editBtn;
    @FXML
    public Button newBtn;
    @FXML
    public TableColumn DateColumn;
    @FXML
    public TableColumn TitleColumn;
    @FXML
    public TableColumn StateColumn;
    @FXML
    public TableView TasksTv;
    @FXML
    public Label NameLbl;
    @FXML
    public Label DateLbl;
    @FXML
    public Label StateLb;
    @FXML
    public Label TextLbl;

    public void bind(){
        deleteBtn.disableProperty().bind(viewModel.isSelected());
        editBtn.disableProperty().bind(viewModel.isSelected());
        NameLbl.textProperty().bind(viewModel.getTitle());
        DateLbl.textProperty().bind(viewModel.getDueDate());
        StateLb.textProperty().bind(viewModel.getState());
        TextLbl.textProperty().bind(viewModel.getDescription());
        TasksTv.setItems(viewModel.getTasks());
        TasksTv.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Task>() {
            @Override
            public void changed(ObservableValue<? extends Task> observable, Task oldValue, Task newValue) {
                viewModel.setSelectedTask(newValue);
            }
        });
    }

    @FXML
    public void initialize(){
        DateColumn.setCellValueFactory(new PropertyValueFactory<>("dueDate"));
        TitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        StateColumn.setCellValueFactory(new PropertyValueFactory<>("state"));
    }


    public void backAction(ActionEvent actionEvent) {
        viewModel.back();
    }

    public void deleteAction(ActionEvent actionEvent) {
        viewModel.removeAction();
    }

    public void editAction(ActionEvent actionEvent) {
        viewModel.editAction();
    }

    public void newAction(ActionEvent actionEvent) {
        viewModel.newAction();
    }

    @Override
    public String getTitle() {
        return "Projects";
    }
}
