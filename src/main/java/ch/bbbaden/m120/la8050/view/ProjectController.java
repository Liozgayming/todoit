package ch.bbbaden.m120.la8050.view;

import ch.bbbaden.m120.la8050.viewmodel.IViewModel;
import ch.bbbaden.m120.la8050.viewmodel.ProjectViewModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class ProjectController extends Controller<ProjectViewModel> {

    @FXML
    private TextField nameTb;
    @FXML
    public Button cancelBtn;
    @FXML
    public Button safeBtn;

    @Override
    public String getTitle() {
        return "Edit Project";
    }

    @Override
    public void bind() {
        nameTb.textProperty().bindBidirectional(viewModel.getTitle());
    }

    public void cancelAction(ActionEvent actionEvent) {
        viewModel.CancelAction();
    }

    public void safeAction(ActionEvent actionEvent) {
        viewModel.SafeAction();
    }
}
